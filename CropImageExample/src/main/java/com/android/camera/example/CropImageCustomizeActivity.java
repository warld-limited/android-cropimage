package com.android.camera.example;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

import com.android.camera.CropImageBaseActivity;
import com.android.camera.CropImageView;

public class CropImageCustomizeActivity extends CropImageBaseActivity {

    @Override
    protected Button getSaveButton() {
        return (Button) findViewById(R.id.save);
    }

    @Override
    protected Button getDiscardButton() {
        return (Button) findViewById(R.id.discard);
    }

    @Override
    protected CropImageView getCropImageView() {
        return (CropImageView) findViewById(R.id.image);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_crop_image_customize;
    }
}
