package com.android.camera;

import android.os.Bundle;
import android.view.Window;
import android.widget.Button;

/**
 * Created by watyaa on 16/02/08.
 */
public class CropImageActivity extends CropImageBaseActivity {

    @Override
    public void onCreate(Bundle icicle) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(icicle);
    }

    @Override
    protected Button getSaveButton() {
        return (Button) findViewById(R.id.save);
    }

    @Override
    protected Button getDiscardButton() {
        return (Button) findViewById(R.id.discard);
    }

    @Override
    protected CropImageView getCropImageView() {
        return (CropImageView) findViewById(R.id.image);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.cropimage;
    }
}
