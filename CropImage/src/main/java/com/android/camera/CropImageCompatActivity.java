package com.android.camera;

import android.widget.Button;

/**
 * Created by watyaa on 16/02/08.
 */
public class CropImageCompatActivity extends CropImageBaseCompatActivity {

    @Override
    protected Button getSaveButton() {
        return (Button) findViewById(R.id.save);
    }

    @Override
    protected Button getDiscardButton() {
        return (Button) findViewById(R.id.discard);
    }

    @Override
    protected CropImageView getCropImageView() {
        return (CropImageView) findViewById(R.id.image);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.cropimage;
    }
}
