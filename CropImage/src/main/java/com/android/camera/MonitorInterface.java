/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.camera;

public interface MonitorInterface {
    public interface LifeCycleListener {
        void onActivityCreated(MonitorInterface activity);
        void onActivityDestroyed(MonitorInterface activity);
        void onActivityStarted(MonitorInterface activity);
        void onActivityStopped(MonitorInterface activity);
    }

    public static class LifeCycleAdapter implements LifeCycleListener {
        public void onActivityCreated(MonitorInterface activity) {
        }

        public void onActivityDestroyed(MonitorInterface activity) {
        }

        public void onActivityStarted(MonitorInterface activity) {
        }

        public void onActivityStopped(MonitorInterface activity) {
        }
    }

    public void addLifeCycleListener(LifeCycleListener listener);

    public void removeLifeCycleListener(LifeCycleListener listener);
}
